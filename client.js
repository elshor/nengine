//nengine client
/* jshint node:true */
var ncore = require("nnatural/ncore.js");
var socket = null;
var frames = {};
var gurl = null;
var fLogRequest = false;
var fLogResponse = false;

function emit(frame,message,args){
	if(ncore.isFrame(frame)){
		//frame is an nnatural frame - use aysnc report
		ncore.asyncReport(frame,message,args);
	}else{
		if(message === "error []"){
			//treat "error []" and special case "error" event
			frame.emit.apply(frame,['error'].concat(args));
		}else{
			frame.emit.apply(frame,[message].concat(args));
		}
	}
}

function end(frame){
	if(ncore.isFrame(frame)){
		//frame is an nnatural frame - use aysnc report
		ncore.asyncComplete(frame);
	}else{
		frame.emit('end');
	}
}

function connect(url,frame,cb){
	gurl = url;
	socket = require('engine.io-client')(url,{transports:["polling"]});
	socket.on('open', function(){
		socket.on('message', function(data){reportMessage(data);});
		socket.on('close', function(){
			socket = null;
			if(frame){
				emit(frame,"closed",[]);
				end(frame);
			}
		});
		if(frame){
			emit(frame,"opened",[]);
		}
		if(cb){
			cb(null,socket);
		}
	});
}

function reportMessage(data){
	if(fLogResponse){
		console.log("<",data);
	}
	var parsed = {};
	try{
		parsed = JSON.parse(data);
	}catch(e){
		console.log("Error - parsing message from NEngine server - ",e);
		console.log(data);
		return;
	}
	if(frames[parsed[0]]){
		if(parsed.length === 1){//complete message
			end(frames[parsed[0]]);
			delete frames[parsed[0]];
		}else{
			if(frames[parsed[0]]){
				emit(frames[parsed[0]], parsed[1], parsed[2] || []);
			}else{
				console.log("Message after completion");
				console.log(data);
			}
		}
	}
}

/**
 * Kill all user requests that are currently running
 * @param {[[Type]]} userkey [[Description]]
 */
function killUser(userkey){
	var out = `[0,"kill",["${userkey}"]]`;
	if(socket){
		socket.send(out);
	}else{
		if(!gurl){
			console.error("url was not defined. Cannot kill user");
			return;
		}
		connect(gurl,null,function(){
			socket.send(out);
		});
	}
}

function disconnect(){
	if(socket){
		socket.close();
	}
	socket = null;
}

/**
 * dispatch a message to server
 * @param   {object} frame   origin frame
 * @param   {String} message the message with [] replacing all arguments e.g. "say []"
 * @param   {Array}  args    array of arguments
 * @param   {Object} context a context object in the format {'the user':{...},'the location':{..},...}
 * @returns {Number} the message id
 */
function dispatch(frame,message,args,context){
	frames[frame._id] = frame;
	var out = "[" + frame._id + ", \"" + ncore.nValue(message) + "\", " + JSON.stringify(ncore.nValue(args));
	if(context){
		out += ("," + JSON.stringify(context));
	}
	out += "]";
	if(socket){
		if(fLogRequest){
			console.log("> " + message);
		}
		socket.send(out);
		return frame._id;
	}else{
		if(!gurl){
			console.log("url was not defined. Cannot dispatch message ",message);
			return null;
		}
		connect(gurl,frame,function(){
			if(fLogRequest){
				console.log("> " + message);
			}
			socket.send(out);
		});
		return frame._id;
	}
}
module.exports = {
	connect : connect,
	dispatch : dispatch,
	disconnect:disconnect,
	killUser : killUser,
	logRequest : function(flag){fLogRequest = flag;},
	logResponse : function(flag){fLogResponse = flag;},
};