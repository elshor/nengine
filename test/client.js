//Tue, 26 May 2015 12:19:10 GMT
var ncore = require('nnatural/ncore');
function i(id,frame){if(frame[id]!== undefined){return frame[id];}ncore.getContextFrame(frame)[id] = 
require('nnatural/acore')['json']();return frame[id];}var v=ncore.nValue;
var local = {
	'isFrame' : true}
;
var thisModule = {
	'connect' : function(parent, loc){
	var frame = ncore.Frame(parent,null,parent,'jsaction');
	frame._loc = loc;
	var ret = (function(frame){require('nnatural-engine/client').connect('http://127.0.0.1:8083',frame);return false;})(frame);
	if(ret ===true){return ncore.complete(frame);}else{return ncore.async(frame);}},
	'disconnect' : function(parent, loc){
	var frame = ncore.Frame(parent,null,parent,'jsaction');
	frame._loc = loc;
	var ret = (function(frame){require('nnatural-engine/client').disconnect();return true;})(frame);
	if(ret ===true){return ncore.complete(frame);}else{return ncore.async(frame);}},
	'hello world' : function(parent, loc){
	var frame = ncore.Frame(parent,null,parent,'jsaction');
	frame._loc = loc;
	var ret = (function(frame){require('nnatural-engine/client').dispatch(frame,'hello world',[]);return false;})(frame);
	if(ret ===true){return ncore.complete(frame);}else{return ncore.async(frame);}},
	'hello []' : function(You, parent, loc){
	var frame = ncore.Frame(parent,null,parent,'jsaction');
	frame.You = You;
	frame._loc = loc;
	var ret = (function(You, frame){require('nnatural-engine/client').dispatch(frame,'hello []',[You]);return false;})(
	i('You', frame), frame);
	if(ret ===true){return ncore.complete(frame);}else{return ncore.async(frame);}},
	'main' : function(parent, loc){
	var frame = ncore.Frame(parent,null,local,'action');
	frame._loc = loc;
	frame.this = frame;
	return ncore.doAll([
		function(frame){return thisModule['connect'](frame, 'connect(client:14)');},
		function(frame){return ncore.doInOrder([
			function(frame){return thisModule['hello world'](ncore.Frame(frame, {
				'hello world' : function(parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler');
					frame._origin = origin;
					frame._loc = loc;
					return ncore.doAll([
						function(frame){return require('nnatural/acore')['log []']("Hello world from client", frame, 'log [](client:18)');}], frame, '*on hello world(client:17)');}},frame), 'hello world(client:16)');},
			function(frame){return thisModule['hello []']("funny", ncore.Frame(frame, {
				'hello []' : function(You, parent, reportTo, origin, loc){
					var frame = ncore.Frame(reportTo, null, parent,'message-handler');
					frame._origin = origin;
					frame._loc = loc;
					frame.You = You;
					return ncore.doAll([
						function(frame){return require('nnatural/acore')['log []'](v(["hello ",i('You', frame)," from client"]).join(''), frame, 'log [](client:21)');}], frame, '*on hello [](client:20)');}},frame), 'hello [](client:19)');},
			function(frame){return require('nnatural/acore')['log []']("after first round", frame, 'log [](client:22)');},
			function(frame){return ncore.forEach('X', require('nnatural/acore')['[] to []'](1, 100, frame), function(frame){return ncore.doAll([
				function(frame){return thisModule['hello []'](i('X', frame), frame, 'hello [](client:24)');}], frame, '*for each [] in [](client:23)');}, frame, '*for each [] in [](client:23)');},
			function(frame){return require('nnatural/acore')['log []']("after second round", frame, 'log [](client:25)');},
			function(frame){return thisModule['disconnect'](frame, 'disconnect(client:26)');}], frame, 'do in order(client:15)');}], frame, '*main(client:13)');}};
module.exports = thisModule;
ncore.dispatch(module.exports.main,[ncore.isNEngineContext()? null : 
require('nnatural/acore')['shell'](null),null]);