#nNatural Engine
An nNatural engine receiving requests from clients and executing them

## About nNatural Engine
nNatural is a new computer language aiming to resemble as much as possible natural language such as English. The engine runs as a process listening on a designated port for client requests. Clients can request execution of an nNatural action and the action is executed on the nengine server. This allows remote invokation of nnatural actions from withing javascript code. The remotely activated action emits reports that are sent back to the client.

## Example
On server side:
```
$ nengine -p 8083 -count 10 server-module
```
On client from javascript code:

Initiating connection:
```
require('nnatural-engine/client').connect('http://127.0.0.1:8083');
```
Calling action
```
require('nnatuarl-engine/client').dispatch(frame,'server action',[arg1,arg2]);
```
reports will then be handled by frame

Disconnecting
```
require('nnatural-engine/client').disconnect();
```

## Protocol
The protocol used in websockets with long polling fallback - using the engine.io library. Each message is a json message.

###Initiation
Client opens connection on server. The server url may contain the following url query parameters:
* key - key used to access the server service. It is up to the application to build the logic of handling the key
* client - Client id of the client. It is up to the application to build the logic of handling the clientid

###Request
Client sends an array with the following format: 
```
[frameid,message,[arguments]]
```
###Reports
Server sends an array with the following format: 
```
[frameid,report-message,[arguments]]
```
where frameid is the frameid of the client request

###Action Completion
Server sends and array with the following format: 
```
[frameid]
```
where frameid is the frameid of the client request

##Server Side Usage
on server side
```
$ nengine [OPTIONS] module1 module2 ...
```
###Options
* -p port to listen to
* -count number of child processes to spawn for action execution. Default 1.

## Deviation from regular nnatural flow
As a rule working with engine from client is just like invoking an action from within the code. The differneces are:
* With nengine, there are no context variables (as of now. This feature may be added in the future)
* With nengine, only json objects can be passed as action arguments, not components.
* When count is higher than 1, each dispatched action can be handled by a different process. There is not stickyness and therefore there is no state. Actions are completely stateless

##Install
```
$ npm install -g nnatural-nengine
```
##License
MPL 2.0