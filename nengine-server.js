#!node
//nengine server
/* jshint node:true */

//require moduls
var path = require('path');
var engine = require('engine.io');
var _ = require('underscore');
var url = require('url');
var exiting = false;//true when about to exit the server
//extract args from command line
var opts = require('minimist')(process.argv.slice(2),{
	default:{
		count:1,
		delay:20,
		'max-ms':600000,
		'max-count':100,
		port:8083},
	alias:{
		h : "help",
		p:"port",
		m:'max-ms',
		c:"count"}});
//if help option specified, print help message and exit
if(opts.help){
	console.info("Usage: node nengine-server [OPTIONS] modules");
	console.info("\t -h --help \tprint this help message");
	console.info("\t --cert \tssl certificate");
	console.info("\t --key \tssl key");
	console.info("\t --delay \tdelay in milliseconds after all workers are busy before retrying");
	console.info("\t -p --port \tport nengine should listen to");
	console.info("\t -c --count \tnumber of child processes to spawn");
	console.info("\t -m --max-ms \tmaximum ms for each process to run");
	console.info("\t --max-count \tmaximum number of processes - when busy add processees until max-count");
	return 0;
}

//start websocket listener
var http = null;
if(opts.cert && opts.key){
	http = require('https').createServer({cert:opts.cert,key:opts.key}).listen(opts.port);
}else{
	http = require('http').createServer().listen(opts.port);

var server = engine.attach(http);
console.info(`${new Date().toISOString()} [${opts.port}] : OPEN ${JSON.stringify(opts._)}`);

//handle connection
server.on('connection', function (socket) {
	var user = {};
	if(url.parse(socket.request.url,true).query.key){
		user.key = url.parse(socket.request.url,true).query.key;
	}
	if(url.parse(socket.request.url,true).query.client){
		user.clientid = url.parse(socket.request.url,true).query.client;
	}
	
	user._type="client-info";
	console.info(`${new Date().toISOString()} [${socket.id}] : OPEN`);
	socket.on('message', function(data){
		var json = [];
		var key = user.key || null;
		try{
			json = JSON.parse(data);
			
			if(!Array.isArray(json)){
				throw "Input format is incorrect - should be an array";
			}
			if(json.length > 0 && json[0] === 0){
				handleSystemRequest(json,socket.id);
				return;
			}
			if(json.length < 2){
				throw "Input json missing message";
			}
			
			if(json.length < 3){
				//only message sent - add empty array in arguments place
				json.push([]);
			}
			if(json.length < 4){
				//missing context
				json.push({});
			}
			if(!json[3]['the Clientid']){
				//default clientid is user clientid
				json[3]['the Clientid'] = user.clientid || "";
			}
			if(!json[3]['the Key']){
				//default clientid is user clientid
				json[3]['the Key'] = user.key || "";
			}else{
				//key exists - set it (override user.key if exists)
				key = json[3]['the Key'];
			}

			if(!json[3]['the ConnectionName']){
				//this is a name used for logging
				json[3]['the ConnectionName'] = socket.id;
			}

			console.info(`${new Date().toISOString()} [${json[0]}@${json[3]['the ConnectionName']}] : REQ ${json[1]}`);
		}catch(e){
			var name = json && json[3] && json[3]['the ConnectionName']?json[3]['the ConnectionName'] : socket.id;
			console.info(new Date().toISOString(), "[" + name + "] : REQEXPTN ",e);
		}
		var worker = assignWorker(socket,JSON.stringify(json),key);
		if(!worker){
			//there is no free worker - if did not reach max-count then create a new proc
			if(Object.keys(procs).length < opts['max-count']){
				createWorker();
				worker = assignWorker(socket,JSON.stringify(json),key);
			}
		}
		if(!worker){
			//exceeding max-procs - try again in opts.delay ms
			var repeater = setInterval(function(){
				worker = assignWorker(socket,JSON.stringify(json),key);
				if(worker){
					clearInterval(repeater);
				}},opts.delay);}});

	socket.on('error',function(error){
		console.info(new Date().toISOString(), "[",socket.id, "] : ERR ",error.toString());});
	socket.on('close', function(){
		console.info(new Date().toISOString(), "[",socket.id, "] : CLOSE ");
	});
});
}

//build worker pool
var child_process = require('child_process');
var procs = {};
for(var i=0;i<opts.count;++i){
	createWorker();
}

/**
 * Handle system requests - that is requests that are intended for the nengine-server. They are not dispatched
 * to workers
 * @param {Array} jsonMessage A parsed message request in form on an array with first element equal zero
 */
function handleSystemRequest(jsonMessage,socketid){
	try{
		if(jsonMessage[1] === 'kill'){
			var key = jsonMessage[2][0];
			var count = 0;
			console.info(`${new Date().toISOString()} [${socketid}] : KILLREQ`);
			for(var x in procs){
				if(procs[x].key === key){
					count++;
					console.info(`${new Date().toISOString()} [${socketid}] : KILLRES found pid ${x}`);
					killWorker(x);
				}
			}
			console.info(`${new Date().toISOString()} [${socketid}] : KILLEND found ${count} processes`);
		}else{
			console.info(`${new Date().toISOString()} [${socketid}] : SYSREQ UNKNOWN ${jsonMessage[1]}`);
		}
	}catch(e){
		console.info(`${new Date().toISOString()} [${socketid}] : SYSREQ FAIL ${jsonMessage}`);
	}
}

function createWorker(){
	var proc = child_process.fork([__dirname+"/nengine-proc"],process.argv.slice(2),{stdio:"inherit"});
	var pid = proc.pid;
	console.info(new Date().toISOString(), "[",pid, "] : CHILD ");
	proc.on('exit',function(code){
		console.info(new Date().toISOString(), "[",pid, "] : CHILD CLOSED ");
		if(!exiting){
			createWorker();
		}
	});
	proc.on('message',function(data){
		if(data && getWorker(pid) && getWorker(pid).socket){
			getWorker(pid).socket.send(data);
		}else{
			unassign(pid);
		}});
	procs[pid]={proc:proc};
}

/**
 * Assign a socket to a child process
 * @param   {Socket}   socket  the socket
 * @param   {string}   message the json message
 * @param   {string}   key     the key used to dispatch the request. This is required when need to kill the process
 * @returns {[[Type]]} [[Description]]
 */
function assignWorker(socket,message,key){
		for(var x in procs){
			if(!procs[x].socket){
				procs[x].socket = socket;
				procs[x].message = message;
				procs[x].key = key;
				procs[x].start = Date.now();
				procs[x].timer = setTimeout(killer.bind(procs[x]),opts['max-ms']);
				procs[x].proc.send(message);
				return procs[x];
			}
		}
		return null;
}

function killer(){
	killWorker(this.proc.pid);
	createWorker();

}

function getWorker(pid){
	return procs[pid];
}
function getWorkerSocket(pid){
	return procs[pid].socket;
}
function unassign(pid){
	if(procs[pid]){
		procs[pid].socket = null;
		procs[pid].message = null;
		procs[pid].key = null;
		clearTimeout(procs[pid].timer);
		procs[pid].timer = null;
	}
}

function removeWorker(pid){
	delete procs[pid];
}

function killWorker(pid){
	try{
		process.kill(pid);
		console.info(new Date().toISOString(), "[",pid, "] : CHILD KILLED");
	}catch(e){
		console.info(new Date().toISOString(), "[",pid, "] : CHILD KILL FAILED " + e.message);
	}
}
//var ncore = require("./ncore.js");
//var _ = require("underscore");
//var url = require('url');
var sockets = {};

//kill all child procs before exiting
process.on('SIGTERM',exitServer.bind(this,'SIGTERM'));
process.on('exit',exitServer.bind(this,'EXIT'));
//process.on('SIGTERM',exitServer.bind(this,'SIGTERM'));
function exitServer(why){
	console.trace("Here I am!");
	exiting = true;
	console.info(new Date().toISOString(), "[",opts.port, "] : " + why);
	if(http){
		http.close();
	}
	for(var x in procs){
		killWorker(x);
	}
}