//nengine-proc.js

/* jshint node:true */
var ncore = require("nnatural/ncore.js");
var _ = require("underscore");
var url = require('url');
var path = require('path');

//read command line arguments.
var opts = require('minimist')(process.argv.slice(2),{
	default:{
		count:1,
		port:8083},
	alias:{
		h : "help",
		p:"port",
		c:"count"}});

//libs are the modules the client may execute actions they contain
var libs = _.map(opts._,function(name){return require(path.resolve(process.cwd(),name));});

//message handler
process.on('message', function(data) {
	var parsed = null;
	try{
		parsed = JSON.parse(data);
		var args = parsed[2] || [];//if args are not specified then fill in empty args list
		var context = parsed[3] || {};//if context not specified then fill in empty context
		var func = getFunctionInModules(parsed[1],libs);
		if(!func){
			console.log(new Date().toISOString(), "[] : ERRNOMATCH ",parsed[1]);
			process.send("[" + parsed[0] + ", \"error []\", [" + JSON.stringify('Requested action does not exist') + "]]");

			return;
		}
		ncore.dispatch(func,args.concat(new ReportDispatcher(parsed[0],context),"proxy"));
		return;
	}catch(e){
		console.log(new Date().toISOString(), "[] : RESERR ",data);
		console.error("error caught in messageHandler - ",e.toString());
		var stack = e.stack.split("\n");
		for(var i=0;i<stack.length;++i){
			console.error(stack[i]);
		}
		console.error(data);
		console.error("-------------------------------------------------------------------");
		
		if(parsed && typeof parsed[0] !== undefined){
			process.send("[" + parsed[0] + ", \"error []\", [" + JSON.stringify(e.toString()) + "]]");
			process.send("["+ parsed[0]+ "]");//complete action
		}else{
			process.send("[" + 0 + ", \"error []\", [" + JSON.stringify(e.toString()) + "]]");
		}
	}});


function reportInfo(message,args){
	return "";
}

function getFunctionInModules(name,modules){
	for(var i=0;i<modules.length;++i){
		if(typeof modules[i][name] === "function"){
			return modules[i][name];
		}
	}
	return null;
}

function ReportDispatcher(frameid, context){
	this.frameid = frameid;
	_.extend(this,context);
	
	//load context object into global _context
	global._context = [];
	for(var x in context){
		var match = x.match(/^the\s+(.+)/);
		if(match){
			global._context.push({scope:'transient',tag:match[1],target:context[x]});
		}else{
			global._context.push({scope:'transient',name:x,target:context[x]});
		}
	}
}

ReportDispatcher.prototype.isReportDispatcher = true;

ReportDispatcher.prototype.isFrame = true;

ReportDispatcher.prototype._type = "proxy";

ReportDispatcher.prototype.report = function(message,args){
	
	if(message === "key []"){//intercept key message to store key
		this["the Key"] = ncore.nValue(args[0]);
	}
	
	var text = "";
	try{
		text = "[" + this.frameid + ", " + JSON.stringify(ncore.nValue(message)) + ", " + JSON.stringify(_.map(args,ncore.nValue)) + "]";
	}catch(e){
		console.error("Cannot report '" + message + "' - ",e.toString());
		return;
	}
	if(message === "error []"){
		console.log(`${new Date().toISOString()} [${this.frameid}@${this['the ConnectionName']}] : RESERR ${args[0]}`);
	}else{
		console.log(`${new Date().toISOString()} [${this.frameid}@${this['the ConnectionName']}] : RES ${message} ${reportInfo(message,args)}`);
	}
	process.send(text);
};

ReportDispatcher.prototype.complete = function(){
	console.log(`${new Date().toISOString()} [${this.frameid}@${this['the ConnectionName']}] : REQE ended`);
	process.send("["+this.frameid + "]");
	process.send(null);//end this execution
};

ReportDispatcher.prototype.log = function(type,message){
	console.log(new Date().toISOString() + " " + "[",this.frameid,"@",this['the ConnectionName'],"] : " + type +  " " + message);
};

//module loading and executing

function dispatch(path, actionName,args){
	var handler = {
		on : function(message,func){
			this._events[message] = func;
		},
		_events : {}
	};
	ncore.dispatch(require(path)[actionName],args);
}

function ModuleContext(filename, frame){ 
	return {
		nParentFrame : frame, //used to propogate reports to the script initiator
		console : console,
		process : process,
		global : {},
		setTimeout : setTimeout,
		setInterval : setInterval,
		isNScript : true,
		Buffer : Buffer,
		ArrayBuffer : ArrayBuffer,
		require : function(id){
			return require(id); //TODO potentialy dangerous - exposing modules like fs may be dangerous
		},
		_report : function(message,args,loc){
			ncore.asyncReport(frame, message,args);
		},
		_complete : function(){
			ncore.asyncComplete(frame);
		}
	};
}

function run(code, name, frame){
	var main = ModuleContext(name,frame);
	var vm = require('vm');
	var script = new vm.Script(code,name);
	try{
		script.runInNewContext(main);
	}catch(e){
		ncore.asyncReport(frame,"error []",[e.toString()]);
		ncore.asyncReport(frame,"exception [] in script execution with stack []",[e.toString(),e.stack]);
		ncore.asyncComplete(frame);
	}
}
