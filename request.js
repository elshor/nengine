//request.js
var client = require('./client');
var _ = require('underscore');

const EventEmitter = require('events');
var id = 1;

	/**
	 * Initialize a request to the dodido server. If args is provided then treat the request as an nnatural
	 * request. If args are not null then tream the request as an action using run script
	 * @param {[[Type]]} message [[Description]]
	 * @param {[[Type]]} args    [[Description]]
	 * @param {Array}    context The context in which the request should be executed
	 */
function Request(message,args,context){
	this._id = id++;
	client.dispatch(this,message,args,context);
}
_.extend(Request.prototype,EventEmitter.prototype);

module.exports = Request;